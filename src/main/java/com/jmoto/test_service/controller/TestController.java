package com.jmoto.test_service.controller;

import com.jmoto.test_service.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TestController
{
    private static Logger log = LoggerFactory.getLogger(TestController.class);

    @GetMapping(value = "/echoStudentName/{name}")
    public String echoStudentName(@PathVariable(name = "name") String name)
    {
        log.info("service consumed");
        return "hello  <strong style=\"color: red;\">" + name + " </strong> Responsed on : " + new Date();
    }

    @GetMapping(value = "/getStudentDetails/{name}")
    public User getStudentDetails(@PathVariable(name = "name") String name)
    {
        return new User(System.currentTimeMillis()+"",name, "Pune", "MCA");
    }
}

