package com.jmoto.test_service.model;

public class User
{
    String id;
    String name;
    String address;
    String pwd;

    public User(String id, String name, String address, String cls) {
        super();
        this.id = id;
        this.name = name;
        this.address = address;
        this.pwd = cls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPwd() {
        return pwd;
    }
}